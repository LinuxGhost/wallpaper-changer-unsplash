#!/usr/bin/python

#####################################################\
##                                                   ###
##  Unsplash-weather-wallpaper.py                      ###
##  v 1.0                                                ###
##  Unsplash Weather Wallpaper Setter                      ###
##  For python 3.x                                           ###
##                                                             ###
##  Sets desktop background to image from Unsplash               ###
##  API based on current weather conditions and                    ###
##  time of day.                                                     ###
##                                                                     ###
##  Using code from: keith24                                           ###
##  (https://gist.github.com/keith24/4b032f5dc50b31e0ea6054fada0298b7) ###
##                                                                     ###
##########################################################################
#########################################################################/

####################### IMPORTS #####################
#Install Unsplash python wrapper 
#https://github.com/salvoventura/pyunsplash

import requests
import secrets
import os
import urllib.request
from os import system
from time import time
import pyunsplash
###########Powered by Unsplash##########################

#https://unsplash.com/developers
pu = pyunsplash.PyUnsplash(api_key=' ')

################## SET THESE VALUES #################

## Location
coordinates = " "

# Get a Dark Sky API key at (https://developer.forecast.io/)
darksky_api_key = " "

#####################################################
#Check if we can connect to the website
def connection():
    try:
        urllib.request.urlopen('https://source.unsplash.com')
        return True
    except:
        return False

print("Checking connection to Unsplash")
if connection():
    
## Get weather data
    print("Getting weather...")
    darksky_res = requests.get(
	     "https://api.forecast.io/forecast/{}/{}"\
	     .format(darksky_api_key,coordinates)).json()
   
## And parse it
# see (https://developer.forecast.io/docs/v2) for more information
    condition = str(darksky_res['currently']['summary']).split(':')).replace(' ','%20')
    sunrise = int(darksky_res['daily']['data'][0]['sunriseTime'])
    sunset = int(darksky_res['daily']['data'][0]['sunsetTime'])

## Get time of day
    now = int(time())
    if abs(now-sunset)<1800:
        altitude = "sunset"
    elif abs(now-sunrise)<1800:
    	altitude = "sunrise"
    elif now-sunrise>0 and now-sunset<0:
    	altitude = "day"
    else:
        altitude = "night"
        condition = "astrophotography"
## Search Unsplash, change the query according to your needs
    search = pu.photos(type_='random',featured=True,orientation='landscape',count=10,query='1920x1080,Nature,{},{}'.format(altitude,condition))
    list = []
    for photo in search.entries:
        list.append(photo.link_download)

## Ensure at least one photo was returned
    if len(list)<1:
        print("No photos found!")
    else:
        print('Getting random picture: ')
	## Pick one
        url = secrets.choice(list)	
        print(url)
	## Set wallpaper ##
	# gnome (untested)
	#system("\
	#	gsettings set org.gnome.desktop.background picture-uri "+url)

	# xfce >4.12.3
  
    #To get monitor Id run xfconf-query -c xfce4-desktop -m
    #go to Desktop Settings and change a wallpaper
    #then change --property /backdrop/screen0/monitorDVI-I-1/workspace0/last-image
    #with yours
    #Change the path according to your needs.
    #I need to test this one in case XDG doesn't work
    #wmctrl -m | grep "Name:" | cut -d ':' -f 2
    desktopenv = os.environ['XDG_CURRENT_DESKTOP']
    if (desktopenv != 'i3'):
        system("\
    		wget -q -O \
	    	    ~/Pictures/Wallpapers/weather-wallpaper.jpg {} && \
		        xfconf-query \
			    --channel xfce4-desktop \
			    --property /backdrop/screen0/monitorDVI-I-1/workspace0/last-image \
                --set ~/Pictures/Wallpapers/weather-wallpaper.jpg"
	            .format(url))
    else:
        system('\
            wget -q -O ~/Pictures/Wallpapers/weather-wall.jpg {} && \
            DISPLAY=:0.0 feh --bg-scale ~/Pictures/Wallpapers/weather-wall.jpg'.format(url))
        print('Wallpaper set.')
else:
    print("Couldn't reach Unsplash; Please check your internet connection...")

#To run the script simply type python3 wallchanger.py
#Or you can set a crontab job to make it automatically like
# */10 * * * * python3 /path/to/script/wallchanger.py
#this will change your wallpaper every 10 minutes
#Note that if your using the free unsplash plan, you only have
#1000 API calls 
#####################################################


